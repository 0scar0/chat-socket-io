import { cleanEnv, port, str } from 'envalid';

const validateEnv = () => {
  cleanEnv(process.env, {
    PG_USER: str(),
    PG_HOST: str(),
    PG_DATABASE: str(),
    PG_PASSWORD: str(),
    PG_PORT: str(),
  });
};

export default validateEnv;

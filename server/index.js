import express from 'express';
import logger from 'morgan';
import con from './db.js';
import pool from './db.js';
import dotenv from 'dotenv';
import validateEnv from './validateEnv.js';
//import { createClient } from '@libsql/client/.';

import { Server } from 'socket.io';
import { createServer } from 'node:http';
import { Socket } from 'engine.io';

dotenv.config();
validateEnv();

const init = async () => {
  const port = process.env.PORT ?? 3000;
  const app = express();
  const server = createServer(app);
  const io = new Server(server, {
    connectionStateRecovery: {},
  });
  con.connectDB();

  io.on('connection', async (socket) => {
    console.log('a user has connected!');

    socket.on('disconnect', () => {
      console.log('a user has disconnected');
    });

    socket.on('chat message', async (msg) => {
      let result = pool.addMessage(msg);
      io.emit('chat message', msg, result.lastInsertRowid);
    });

    if (!socket.recovered) {
      // try {
      const servoffset = socket.handshake.auth.serverOffset;
      const res = pool.getMessages(servoffset);

      console.log(res);
      // res.rows.forEach((row) => {
      //   Socket.emit('chat message', row.content, row.id_msg.toString());
      // });
      // } catch (e) {
      //   console.error(e.message);
      // }
      //
    }
  });

  app.use(logger('dev'));

  app.get('/', (req, res) => {
    res.sendFile(process.cwd() + '/client/index.html');
  });

  server.listen(port, () => {
    console.log(`Server running on port ${port}`);
  });
};

init();

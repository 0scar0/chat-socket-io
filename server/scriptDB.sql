/*CREATE USER serbero WITH ENCRYPTED PASSWORD 'serbero';
GRANT ALL PRIVILEGES ON DATABASE zurak TO serbero;*/
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
create database zurak;
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
CREATE SCHEMA chat;
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
CREATE table chat.messages (
id_msg serial primary key not null,
content varchar(1200) not null,
user_id int4 null,
status int4 default 1 null,
modification_flag int4 default 0 null,
created_at timestamp default current_timestamp null,
updated_at timestamp default current_timestamp null,
deleted_at timestamp null
);
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
create table chat.messages_audit (
id_audit serial primary key not null,
id_msg int4 not null,
content varchar(1200) not null,
user_id int4 null,
status int4 default 0 null,
modification_flag int4 default 1 null,
observations varchar(300) default 'DATA MODIFIED BY USER' null,
action varchar(25) null,
created_at timestamp default current_timestamp null,
updated_at timestamp default current_timestamp null,
deleted_at timestamp null
);
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
CREATE OR REPLACE FUNCTION audit_message_changes()
RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO chat.messages_audit (id_msg, content, user_id, status, modification_flag, observations, created_at, updated_at, action)
        VALUES (NEW.id_msg, NEW.content, NEW.user_id, 0, 1, 'DATA INSERT BY USER', now(), now(), 'INSERT');
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO chat.messages_audit (id_msg, content, user_id, status, modification_flag, observations, created_at, updated_at, action)
        VALUES (NEW.id_msg, NEW.content, NEW.user_id, 0, 1, 'DATA UPDATE BY USER', now(), now(), 'INSERT');
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
CREATE TRIGGER audit_message_changes_trigger
AFTER INSERT OR UPDATE ON chat.messages
FOR EACH ROW
EXECUTE FUNCTION audit_message_changes();
-- ---------------------------------------------------------------------------------------------------------------------------------------------------- --
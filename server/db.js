import dotenv from 'dotenv';
import validateEnv from './validateEnv.js';
import pg from 'pg';
const { Pool } = pg;
dotenv.config();
validateEnv();

// Configura los detalles de tu conexión a PostgreSQL
const pool = new Pool({
  user: process.env.PG_USER,
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  password: process.env.PG_PASSWORD,
  port: process.env.PG_PORT,
});

// Conexion a la BD
const connectDB = async () => {
  try {
    const client = await pool.connect();
    console.log('Connected to PostgreSQL!!!');
    client.release();
  } catch (err) {
    console.error('Error connecting to PostgreSQL', err);
  }
};

// Agregar mensaje a la BD
const addMessage = async (msg) => {
  try {
    await pool.query('insert into chat.messages (content) values ($1) returning *', [msg]);
  } catch (e) {
    console.error(e);
    return;
  }
};

// Consultar todos los mensajes
const getMessages = async (req) => {
  //console.log(req);
  try {
    //const result =
    await pool.query('SELECT id_msg, content FROM chat.messages WHERE status=1 and id_msg > $1 order by id_msg asc', [
      req ?? 0,
    ]);
    //result.toString();
    //console.log(result.rows);
  } catch (e) {
    console.error(e.message);
  }
};

// Exporta el pool para usarlo en otras partes de tu aplicación
export default { pool, connectDB, addMessage, getMessages };
